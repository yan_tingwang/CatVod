let prod = process.env.NODE_ENV === 'production';

module.exports = {
    productionSourceMap: !prod,
};

