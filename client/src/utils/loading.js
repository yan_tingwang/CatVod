import {Loading} from "element-ui";

let loadingInstance = null;
export default {
    showLoad: (msg) => {
        if (loadingInstance == null) {
            loadingInstance = Loading.service({
                    fullscreen: true,
                    lock: true,
                    text: msg,
                    spinner: 'el-icon-loading',
                    background: 'rgba(1, 1, 1, 0.7)',
                },
            );
        } else {
            loadingInstance.text = msg;
        }
    },

    hideLoad: () => {
        loadingInstance.close();
        loadingInstance = null;
    },
};
