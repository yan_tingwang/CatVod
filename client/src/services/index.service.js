import Api from './api';

export default {
    info() {
        return Api.get('info');
    },
};
