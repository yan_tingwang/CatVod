let Sequelize = require('sequelize');
let db = require('../libs/db.lib');

let Video = db.define('video', {
    // 自增ID
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        unique: true,
        allowNull: false,
    },
    // 来源站点
    site: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    // 采集站id
    oid: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    // 采集站分类id
    ocid: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    // 绑定分类
    cid: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    // 标题
    name: {
        type: Sequelize.STRING,
    },
    // tag 标签 这个字段是用来做 查找检索的
    tags: {
        type: Sequelize.STRING(1024),
    },
    // 最后更新时间
    last: {
        type: Sequelize.DATE,
    },
    // 地区
    area: {
        type: Sequelize.STRING,
    },
    // 年代
    year: {
        type: Sequelize.INTEGER,
    },
    // 演员
    actor: {
        type: Sequelize.STRING,
    },
    // 导演
    director: {
        type: Sequelize.STRING,
    },
    // 封面
    pic: {
        type: Sequelize.STRING,
    },
    // 描述
    desc: {
        type: Sequelize.TEXT,
    },
    // 播放列表
    playlist: {
        type: Sequelize.TEXT('long'),
        get() {
            try {
                return JSON.parse(this.getDataValue('playlist'));
            } catch (e) {
                return {};
            }
        },
        set(val) {
            this.setDataValue('playlist', JSON.stringify(val));
        },
    },
}, {
    timestamps: false,
    // 为了全文索引
    engine: 'MYISAM',
    // 设置索引
    indexes: [
        {
            unique: false,
            name: 'last',
            fields: ['last'],
        },
        {
            unique: true,
            name: 'site_oid',
            fields: ['site', 'oid'],
        },
        {
            unique: false,
            name: 'oid',
            fields: ['oid'],
        },
        {
            unique: false,
            name: 'ocid',
            fields: ['ocid'],
        },
        {
            unique: false,
            name: 'cid',
            fields: ['cid'],
        },
        {
            unique: false,
            name: 'site',
            fields: ['site'],
        },
        {
            unique: false,
            name: 'name',
            fields: ['name'],
        },
        {
            type: 'FULLTEXT',
            name: 'tags',
            fields: ['tags'],
        },
    ],
});

// 同步model到table 插入默认数据记录
Video.sync({alter: true}).then(async () => {

});

module.exports = Video;
