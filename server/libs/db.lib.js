let Sequelize = require('sequelize');
let dbConfig = require('../config').db;

let sequelize = new Sequelize(dbConfig.name, dbConfig.user, dbConfig.pwd, {
    host: dbConfig.host,
    port: dbConfig.port,
    dialect: 'mysql',
    pool: dbConfig.pool,
    timezone: '+08:00',
    logging: process.env.NODE_ENV === 'development' || false,
    // logging: false,
});

module.exports = sequelize;
