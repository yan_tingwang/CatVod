let _ = require('lodash');
let config = require('../config');
let siteModel = require('../models/site.model');
let spider = require('../libs/spider.lib');

/**
 * 检测客户端提交过来的数据
 * @param req
 * @param res
 * @returns {*}
 */
function checkSite(req, res) {
    let flag = req.body.flag;
    let name = req.body.name;
    let api = req.body.api;
    let binds = req.body.binds;
    let enable = req.body.enable;

    if (_.isNil(flag) || _.isEmpty(flag)) {
        res.jsonFail('唯一标识不能为空!');
        return null;
    }
    if (_.isNil(name) || _.isEmpty(name)) {
        res.jsonFail('站点名不能为空!');
        return null;
    }
    if (_.isNil(api) || _.isEmpty(api)) {
        res.jsonFail('API不能为空!');
        return null;
    }
    if (_.isNil(binds)) {
        binds = {};
    }
    if (_.isNil(enable)) {
        enable = false;
    }
    return {
        flag: flag,
        name: name,
        api: api,
        binds: binds,
        enable: enable,
    };
}

module.exports = {
    /**
     * 获取所有采集站
     * @param req
     * @param res
     * @param next
     */
    sites(req, res, next) {
        siteModel.findAll().then(cs => {
            res.jsonSuccess(cs);
        }).catch(e => {
            res.jsonFail(e.message);
        });
    },

    /**
     * 增加站点
     * @param req
     * @param res
     * @param next
     */
    add(req, res, next) {
        let result = checkSite(req, res);
        if (_.isNil(result)) {
            return;
        }
        siteModel.findOne({attributes: ['flag'], where: {flag: result.flag}}).then(s => {
            if (s) {
                res.jsonFail(`唯一标识${result.flag}已经存在！`);
                return;
            }
            return siteModel.create(result).then((s) => {
                res.jsonSuccess(s);
            }).catch(e => {
                res.jsonFail(e.message);
            });
        }).catch(e => {
            res.jsonFail(e.message);
        });
    },

    /**
     * 更新站点
     * @param req
     * @param res
     * @param next
     */
    update(req, res, next) {
        let result = checkSite(req, res);
        if (_.isNil(result)) {
            return;
        }
        siteModel.findOne({attributes: ['flag'], where: {flag: result.flag}}).then(s => {
            if (!s) {
                res.jsonFail(`唯一标识${result.flag}不存在！`);
                return;
            }
            s.name = result.name;
            s.appName = result.api;
            s.enable = result.enable;
            s.api = result.api;
            for (let key in result.binds) {
                if (_.isNil(result.binds[key]) || (_.isString(result.binds[key]) && _.isEmpty(result.binds[key]))) {
                    delete result.binds[key];
                }
            }
            s.binds = result.binds;
            return s.save().then((s) => {
                res.jsonSuccess(s);
            }).catch(e => {
                res.jsonFail(e.message);
            });
        }).catch(e => {
            res.jsonFail(e.message);
        });
    },

    /**
     * 获取站点分类绑定信息
     * @param req
     * @param res
     * @param next
     */
    classes(req, res, next) {
        let flag = req.body.flag;
        if (_.isNil(flag) || _.isEmpty(flag)) {
            res.jsonFail('唯一标识不能为空!');
            return;
        }
        siteModel.findOne({where: {flag: flag}}).then(s => {
            if (!s) {
                res.jsonFail(`唯一标识${flag}不存在！`);
                return;
            }
            return spider.siteClass(s.api).then(classes => {
                res.jsonSuccess(classes);
            }).catch(e => {
                res.jsonFail(e.message);
            });
        }).catch(e => {
            res.jsonFail(e.message);
        });
    },

    /**
     * 采集
     * @param req
     * @param res
     * @param next
     */
    cj(req, res, next) {
        let flag = req.body.flag;
        let hour = req.body.hour;
        let page = req.body.page;
        if (_.isNil(flag) || _.isEmpty(flag)) {
            res.jsonFail('唯一标识不能为空!');
            return;
        }
        if (_.isNil(hour)) {
            hour = 24;
        }
        if (_.isNil(page)) {
            page = 0;
        }
        return spider.siteCJTask(flag, hour, page).then(cjInfo => {
            res.jsonSuccess(cjInfo);
        }).catch(e => {
            res.jsonFail(e);
        });
    },


    cjAll(req, res, next) {
        spider.cjAllSite();
        res.jsonSuccess(null, 'task start on server!');
    },

};
